import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from "class-validator";

@ValidatorConstraint({ name: "MatchLength", async: false })
export class MatchLength implements ValidatorConstraintInterface {

  validate(value: any[], args: any) {
    const valid = value && (args.object.number_of_cases === value.length);
    return Promise.resolve(valid);
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} length doesn't match number_of_cases`;
  }

}