import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from "class-validator";

@ValidatorConstraint({ name: "Files", async: false })
export class Files implements ValidatorConstraintInterface {

  validate(value: any, args: any) {
    let valid = true;
    for (const key in value) {
      if (!value[key].content) {
        valid = false;
      }
    }
    return Promise.resolve(valid);
  }

  defaultMessage(args: ValidationArguments) {
    return `Some file is missing content`;
  }

}