require("dotenv").config();
import "reflect-metadata";
import express from "express";
import bodyParser from "body-parser";
import router from "./routes";
import { createConnection } from "typeorm";
import { Request, Response, NextFunction } from "express";
import { errorHandler } from "./services/error-handler";

createConnection()
    .then(connection => {})
    .catch(error => console.log(error));

const app = express();

app.set("port", process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);
app.use(errorHandler);

export default app;