import { Request, Response, NextFunction } from "express";

export let errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  if (err) {
    let status = 500;
    let errors: object[] = [];
    if (err.length) {
      status = 422;
      errors = formatClassValidatorErrors(err);
    } else {
      if (err.status || err.statusCode) {
        status = err.status || err.statusCode;
      }
      else {
        switch (err.name) {
          case "EntityNotFound":
            status = 404;
            break;
          default:
            status = 400;
        }
      }

      errors = [{
        message: err.error ? err.error.message : err.message
      }];
    }
    res.status(status).send({
      errors: errors
    });
  }
};

function formatClassValidatorErrors(validationErrors: any[]) {
  return validationErrors.map((error) => {
    let message = "";
    for (const k in error.constraints) {
      message += error.constraints[k] + ". ";
    }
    return {
      source: error.property,
      message: message
    };
  });
}