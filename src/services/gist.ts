import request from "request-promise";

const rp = request.defaults(getRequestBaseOptions());

function getRequestBaseOptions() {
  return {
    baseUrl: process.env.GITHUB_API_ENDPOINT + "/gists",
    headers: {
      "Authorization": "Basic " +
      Buffer
        .from(process.env.GITHUB_USERNAME + ":" + process.env.GITHUB_PASSWORD)
        .toString("base64"),
      "Content-Type": "application/json",
      "User-Agent": process.env.GITHUB_USERNAME
    },
    json: true,
    method: "",
    uri: "",
    body: {}
  };
}

function call(method: string, path: string, data?: object) {
  const options = getRequestBaseOptions();
  options.method = method;
  options.uri = path;
  options.body = data;

  return rp(options);
}

function post(path: string, data: any) {
  return call("POST", path, data);
}

function get(path: string, data?: object) {
  return call("GET", path, data);
}

function put(path: string, data: object) {
  return call("PUT", path, data);
}

function patch(path: string, data: object) {
  return call("PATCH", path, data);
}

function del(path: string) {
  return call("DELETE", path);
}

export function create(data: object) {
  return post("", data);
}

export function findById(id: string) {
  return get(id);
}

export function getComments(id: string) {
  return get(id + "/comments");
}

export function createComment(id: string, data: object) {
  return post(id + "/comments", data);
}
