import express from "express";
import * as scoreboardsController from "../controllers/scoreboards";

const router = express.Router();

const routeBuilder = (path: String) => {

  router.route(`${path}`)
    .get(scoreboardsController.findAll)
    .post(scoreboardsController.create);

  router.route(`${path}/latest`)
    .get(scoreboardsController.latest);

  return router;

};

export default routeBuilder;