import express from "express";
import * as gistsController from "../controllers/gists";

const router = express.Router();

const routeBuilder = (path: String) => {

  router.route(`${path}`)
    .post(gistsController.create);

  router.route(`${path}/:id`)
    .get(gistsController.findById);

  router.route(`${path}/:id/comments`)
    .get(gistsController.getComments);

  return router;

};

export default routeBuilder;