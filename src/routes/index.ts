import express from "express";
import gists from "./gists";
import scoreboards from "./scoreboards";

const router = express.Router();

router.use(gists("/gists"));
router.use(scoreboards("/scoreboards"));

export default router;