import { Column } from "typeorm";
import { IsNotEmpty, IsDefined, IsEnum, Validate } from "class-validator";
import { Files } from "../validators/Files";
export class Gist {

    @Column()
    description: string;

    @Column()
    public: boolean;

    @Column()
    @IsNotEmpty()
    @Validate(Files)
    files: object;

    constructor(obj?: Partial<Gist>) {
      if (obj) {
        Object.assign(this, obj);
      }
    }

}
