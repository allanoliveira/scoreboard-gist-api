import { Column } from "typeorm";
import { RankItem } from "./RankItem";
import { Entry, entryStatuses } from "./Entry";
import { IsNotEmpty, ValidateNested } from "class-validator";

export class Case {

    @Column("array")
    ranking: RankItem[];

    @Column("array")
    @IsNotEmpty()
    @ValidateNested()
    entries: Entry[];

    constructor(obj?: Partial<Case>) {
      if (obj) {
        Object.assign(this, obj);
      }
    }

    generateRanking() {
      const rankingItems: any = {};
      const solvedProblems: any = {};
      this.entries.forEach((entry) => {
        const rankingItem: RankItem = rankingItems[entry.contestant] || new RankItem({contestant: entry.contestant});

        switch (entry.status) {
          case entryStatuses.Correct:
            rankingItem.problems_solved += 1;
            rankingItem.penalty_time += entry.time;
            solvedProblems[entry.problem] = entry.time;
            break;
          case entryStatuses.Incorrect:
            if (solvedProblems[entry.problem]) {
              rankingItem.penalty_time += entry.time;
            }
            else {
              rankingItem.penalty_time += 20;
            }
            break;
        }
        rankingItems[entry.contestant] = rankingItem;
      });

      this.ranking = Object.keys(rankingItems).map(key => rankingItems[key]);
      this.ranking.sort((a, b) => {
        if (a.problems_solved > b.problems_solved) {
          return -1;
        }
        if (a.problems_solved < b.problems_solved) {
          return 1;
        }
        if (a.penalty_time < b.penalty_time) {
          return -1;
        }
        if (a.penalty_time > b.penalty_time) {
          return 1;
        }
        return a.contestant < b.contestant ? -1 : 1;
      });
    }

}
