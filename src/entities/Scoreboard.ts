import { Entity, ObjectIdColumn, Column, ObjectID, BeforeInsert } from "typeorm";
import { IsNotEmpty, Validate, ValidateNested } from "class-validator";
import { Case } from "./Case";
import { MatchLength } from "../validators/MatchLength";

@Entity()
export class Scoreboard {

    @ObjectIdColumn()
    id: ObjectID;

    @Column("array")
    @IsNotEmpty()
    @Validate(MatchLength)
    @ValidateNested()
    cases: Case[];

    @Column()
    @IsNotEmpty()
    number_of_cases: number;

    constructor(obj?: Partial<Scoreboard>) {
      if (obj) {
        Object.assign(this, obj);
        if (obj.cases) {
          this.cases = [];
          for (let i = 0; i < obj.cases.length; i++) {
            this.cases.push(new Case(obj.cases[i]));
          }
        }
      }
    }

    @BeforeInsert()
    generateRankings() {
      for (let i = 0; i < this.cases.length; i++) {
        this.cases[i].generateRanking();
      }
    }

}
