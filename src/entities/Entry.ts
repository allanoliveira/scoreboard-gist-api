import { Column } from "typeorm";
import { IsNotEmpty, IsDefined, IsEnum } from "class-validator";

export const entryStatuses = {
  Correct: "C",
  Incorrect: "I",
  Request: "R",
  Unjudged: "U",
  Erroneous: "E"
};

export class Entry {

    @Column()
    @IsDefined()
    @IsNotEmpty()
    contestant: number;

    @Column()
    @IsNotEmpty()
    problem: number;

    @Column()
    @IsNotEmpty()
    time: number;

    @Column()
    @IsNotEmpty()
    @IsEnum(entryStatuses)
    status: string;

    constructor(obj?: Entry) {
      if (obj) {
        Object.assign(this, obj);
      }
    }

}
