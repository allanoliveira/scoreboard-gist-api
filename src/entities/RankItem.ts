import { Entity, ObjectIdColumn, Column, ObjectID, ManyToOne } from "typeorm";
import { IsNotEmpty } from "class-validator";
import { Case } from "./Case";

export class RankItem {

    @Column()
    @IsNotEmpty()
    contestant: number;

    @Column()
    @IsNotEmpty()
    problems_solved: number;

    @Column()
    @IsNotEmpty()
    penalty_time: number;

    constructor(obj?: Partial<RankItem>) {
      if (obj) {
        Object.assign(this, obj);
        this.penalty_time = this.penalty_time || 0;
        this.problems_solved = this.problems_solved || 0;
      }
    }

}
