import { Request, Response, NextFunction } from "express";
import { getManager, Entity } from "typeorm";
import { Scoreboard } from "../entities/Scoreboard";
import { validateOrReject } from "class-validator";
import { Entry } from "../entities/Entry";

export let findAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const repo = getManager().getRepository(Scoreboard);
    const scoreboard = await repo.find();
    res.send(scoreboard);
  }
  catch (err) {
    next(err);
  }
};

export let create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const repo = getManager().getRepository(Scoreboard);
    let scoreboard = new Scoreboard(req.body);
    await validateOrReject(scoreboard);
    for (let i = 0; i < scoreboard.cases.length; i++) {
      await validateOrReject(scoreboard.cases[i]);
      for (let j = 0; j < scoreboard.cases[i].entries.length; j++) {
        const entry = new Entry(scoreboard.cases[i].entries[j]);
        await validateOrReject(entry);
      }
    }
    scoreboard = await repo.save(scoreboard);
    res.send(scoreboard);
  }
  catch (err) {
    next(err);
  }
};

export let latest = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const repo = getManager().getRepository(Scoreboard);
    const scoreboard = await repo.findOneOrFail({order: {id: "DESC"}});
    res.send(scoreboard);
  }
  catch (err) {
    next(err);
  }
};