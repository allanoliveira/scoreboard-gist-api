import { Request, Response, NextFunction } from "express";
import * as GistService from "../services/gist";
import { Gist } from "../entities/Gist";
import { validateOrReject } from "class-validator";

export let findById = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const gist = await GistService.findById(req.params.id);
    res.send(gist);
  }
  catch (err) {
    next(err);
  }
};

export let create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const gistObj = new Gist(req.body);
    await validateOrReject(gistObj);
    const gist = await GistService.create(req.body);
    res.send(gist);
  }
  catch (err) {
    next(err);
  }
};

export let getComments = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const gist = await GistService.getComments(req.params.id);
    res.send(gist);
  }
  catch (err) {
    next(err);
  }
};