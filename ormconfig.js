const entities = process.env.NODE_ENV === "test" ? ["src/entities/**/*.ts"] : ["dist/entities/**/*.js"];

module.exports = {
    "type": "mongodb",
    "host": process.env.MONGO_HOST,
    "port": process.env.MONGO_PORT || 27017,
    "username": process.env.MONGO_INITDB_ROOT_USERNAME,
    "password": process.env.MONGO_INITDB_ROOT_PASSWORD,
    "database": process.env.MONGO_INITDB_DATABASE,
    "synchronize": true,
    "logging": false,
    "entities": entities,
    "migrations": [
        "dist/migration/**/*.js"
    ],
    "subscribers": [
        "dist/subscriber/**/*.js"
    ],
    "cli": {
        "entitiesDir": "src/entities",
        "migrationsDir": "src/migration",
        "subscribersDir": "src/subscriber"
    }
};