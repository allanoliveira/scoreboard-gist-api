import request from "supertest";
import app from "../src/app";
import { givenGistData, givenGist, givenGistComment } from "./helpers/database";

describe("POST /gists", () => {

  it("should create a gist", async () => {
    const res = await request(app)
      .post("/gists")
      .send(givenGistData())
      .expect(200);
  });

});

describe("GET /gists/:id", () => {

  it("should get gist by id", async () => {
    const gist = await givenGist();
    const res = await request(app)
      .get("/gists/" + gist.id)
      .expect(200);
  });

  it("should fail for nonexistent gist", async () => {
    const res = await request(app)
      .get("/gists/xyz")
      .expect(404);
  });

});


describe("GET /gists/:id/comments", () => {

  it("should return empty array for gist without comments", async () => {
    const gist = await givenGist();
    const res = await request(app)
      .get("/gists/" + gist.id + "/comments")
      .expect(200);
    expect(res.body).toEqual([]);
  });

  it("should return comments for gist with comments", async () => {
    const gist = await givenGist();
    const comment = await givenGistComment(gist.id);
    const res = await request(app)
      .get("/gists/" + gist.id + "/comments")
      .expect(200);
    expect(res.body).toEqual([comment]);
  });

});
