import request from "supertest";
import app from "../src/app";
import { givenScoreData, givenScore, givenEmptyScoreboardDB } from "./helpers/database";
import { createConnection, Connection } from "typeorm";

let connection: Connection;

beforeAll(async () => {
  jest.useFakeTimers();
  connection = await createConnection();
});

afterAll(async() => {
  await connection.close();
});

beforeEach(async() => {
  jest.useFakeTimers();
  await givenEmptyScoreboardDB();
});

describe("POST /scoreboards", () => {

  it("should create scoreboard", async () => {
    const res = await request(app)
      .post("/scoreboards")
      .send(givenScoreData())
      .expect(200);

    expect(res.body.cases[0].ranking[0]).toBeDefined();
  });

  it("should fail validation for missing number_of_cases", async () => {
    const data = givenScoreData();
    delete data.number_of_cases;
    const res = await request(app)
      .post("/scoreboards")
      .send(data)
      .expect(422);

    const found = res.body.errors.filter((e: any) => e.source === "number_of_cases");
    expect(found.length).toBeTruthy();
  });

  it("should fail validation for missing cases", async () => {
    const data = givenScoreData();
    delete data.cases;
    const res = await request(app)
      .post("/scoreboards")
      .send(data)
      .expect(422);

    expect(res.body.errors[0].source).toBe("cases");
  });

  it("should fail validation if number_of_cases and cases.length don't match", async () => {
    const data = givenScoreData();
    data.number_of_cases++;
    const res = await request(app)
      .post("/scoreboards")
      .send(data)
      .expect(422);

    expect(res.body.errors[0].source).toBe("cases");
  });

  it("should fail validation for entry with invalid status", async () => {
    const data = givenScoreData();
    data.cases[0].entries[0].status = "A";
    const res = await request(app)
      .post("/scoreboards")
      .send(data)
      .expect(422);

    expect(res.body.errors[0].source).toBe("status");
  });


  it("should rank scores for challenge sample input", async () => {
    const res = await request(app)
      .post("/scoreboards")
      .send({
        "number_of_cases": 1,
        "cases": [
          {
            "entries": [
              {
                "contestant": 1,
                "problem": 2,
                "time": 10,
                "status": "I"
              },
              {
                "contestant": 3,
                "problem": 1,
                "time": 11,
                "status": "C"
              },
              {
                "contestant": 1,
                "problem": 2,
                "time": 19,
                "status": "R"
              },
              {
                "contestant": 1,
                "problem": 2,
                "time": 21,
                "status": "C"
              },
              {
                "contestant": 1,
                "problem": 1,
                "time": 25,
                "status": "C"
              }
            ]
          }
        ]
      })
      .expect(200);

    expect(res.body.cases[0].ranking[0]).toEqual({
      "contestant": 1,
      "penalty_time": 66,
      "problems_solved": 2
    });
    expect(res.body.cases[0].ranking[1]).toMatchObject({
      "contestant": 3,
      "penalty_time": 11,
      "problems_solved": 1
    });
  });

  it("should rank scores, example 2", async () => {
    const res = await request(app)
      .post("/scoreboards")
      .send({
        "number_of_cases": 1,
        "cases": [
          {
            "entries": [
              {
                "contestant": 1,
                "problem": 1,
                "time": 10,
                "status": "C"
              },
              {
                "contestant": 2,
                "problem": 2,
                "time": 12,
                "status": "I"
              },
              {
                "contestant": 2,
                "problem": 1,
                "time": 15,
                "status": "C"
              },
              {
                "contestant": 1,
                "problem": 2,
                "time": 20,
                "status": "R"
              },
              {
                "contestant": 3,
                "problem": 2,
                "time": 25,
                "status": "C"
              }
            ]
          }
        ]
      })
      .expect(200);

    expect(res.body.cases[0].ranking[0]).toEqual({
      "contestant": 1,
      "penalty_time": 10,
      "problems_solved": 1
    });
    expect(res.body.cases[0].ranking[1]).toMatchObject({
      "contestant": 3,
      "penalty_time": 25,
      "problems_solved": 1
    });
    expect(res.body.cases[0].ranking[2]).toMatchObject({
      "contestant": 2,
      "penalty_time": 35,
      "problems_solved": 1
    });
  });

});

describe("GET /scoreboards/latest", () => {

  it("should return 404 when there is no scoreboard", async () => {
    const res = await request(app)
      .get("/scoreboards/latest")
      .expect(404);
  });

  it("should fetch latest scoreboard", async () => {
    const scoreboard = await givenScore();
    const res = await request(app)
      .get("/scoreboards/latest")
      .expect(200);

    expect(res.body.id).toMatch(scoreboard.id.toString());
    expect(res.body.cases.length).toEqual(scoreboard.cases.length);
  });

});
