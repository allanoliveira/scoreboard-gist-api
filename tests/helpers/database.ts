import faker from "faker";
import * as Gist from "../../src/services/gist";
import { Scoreboard } from "../../src/entities/Scoreboard";
import { getManager, getConnection, createConnection } from "typeorm";

const entryStatus = ["C", "I", "R", "U", "E"];

const randPositiveNumber = (max: number) => {
  return Math.floor(Math.random() * (max)) + 1;
};

const randNaturalNumber = (max: number) => {
  return Math.floor(Math.random() * (max + 1));
};

export async function givenEmptyScoreboardDB() {
  const scoreboards = await getManager().getRepository(Scoreboard).find();
  await getManager().getRepository(Scoreboard).remove(scoreboards);
}

export let givenGistData = () => {
  return {
    "description": faker.lorem.sentence(),
    "public": true,
    "files": givenFilesData()
  };
};

export let givenFilesData = (quantity: number = randNaturalNumber(5)) => {
  const files: any = {};
  for (let i = 0; i < quantity; i++) {
    files[faker.system.fileName()] = {
      "content": faker.lorem.paragraphs()
    };
  }
  return files;
};

export let givenGistCommentData = () => {
  return {
    "body": faker.lorem.sentence()
  };
};

export let givenGist = async () => {
  return await Gist.create(givenGistData());
};

export let givenGistComment = async (gistId: string) => {
  return await Gist.createComment(gistId, givenGistCommentData());
};

export let givenEntryStatus = () => {
  return entryStatus[randNaturalNumber(entryStatus.length - 1)];
};

export let givenEntryData = () => {
  return {
    "contestant": randPositiveNumber(100),
    "problem": randPositiveNumber(9),
    "time": randPositiveNumber(100),
    "status": givenEntryStatus()
  };
};

export let givenCaseData = (number_of_entries?: number) => {
  number_of_entries = number_of_entries || randPositiveNumber(10);
  const entries = [];
  for (let i = 0; i < number_of_entries; i++) {
    entries.push(givenEntryData());
  }
  return {
    "entries": entries.sort((a, b) => {
      return (a.time < b.time) ? -1 : 1;
    })
  };
};

export let givenScoreData = () => {
  const n_cases = randPositiveNumber(10);
  const cases = [];
  for (let i = 0; i < n_cases; i++) {
    cases.push(givenCaseData());
  }
  return {
    "number_of_cases": n_cases,
    "cases": cases
  };
};

export let givenScore = async () => {
  const scoreboard = new Scoreboard(<any> givenScoreData());
  return await getManager().getRepository(Scoreboard).save(scoreboard);
};