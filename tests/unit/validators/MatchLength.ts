import { MatchLength } from "../../../src/validators/MatchLength";
import { givenScoreData } from "../../helpers/database";

let validator;
beforeEach(() => {
  validator = new MatchLength();
});

describe("MatchLength validator", () => {

  it("should succeed validation for cases.length === number_of_cases", async () => {
    const scoreData = givenScoreData();
    expect(await validator.validate(scoreData.cases, {object: {number_of_cases: scoreData.number_of_cases}})).toBe(true);
  });

  it("should fail validation for cases.length < number_of_cases", async () => {
    const scoreData = givenScoreData();
    expect(await validator.validate(scoreData.cases, {object: {number_of_cases: scoreData.number_of_cases + 1}})).toBe(false);
  });

  it("should fail validation for cases.length > number_of_cases", async () => {
    const scoreData = givenScoreData();
    expect(await validator.validate(scoreData.cases, {object: {number_of_cases: scoreData.number_of_cases - 1}})).toBe(false);
  });

  it("should fail validation for missing number_of_cases", async () => {
    const scoreData = givenScoreData();
    expect(await validator.validate(scoreData.cases, {object: {}})).toBe(false);
  });

});