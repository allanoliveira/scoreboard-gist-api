import { Files } from "../../../src/validators/Files";
import { givenFilesData } from "../../helpers/database";

let validator;
beforeEach(() => {
  validator = new Files();
});

describe("Files validator", () => {

  it("should succeed validation for proper data", async () => {
    const filesData = givenFilesData();
    expect(await validator.validate(filesData)).toBe(true);
  });

  it("should fail validation for file missing content", async () => {
    const filesData = {
      "hello.js": {}
    };
    expect(await validator.validate(filesData)).toBe(false);
  });

  it("should fail validation for file empty content", async () => {
    const filesData = {
      "hello.js": {
        "content": ""
      }
    };
    expect(await validator.validate(filesData)).toBe(false);
  });

});