import { Case } from "../../../src/entities/Case";

describe("generateRanking()", () => {

  it("should generate ranking for sample input", () => {
    const caseObj = new Case({
      ranking: [],
      entries: [
        {
          "contestant": 1,
          "problem": 2,
          "time": 10,
          "status": "I"
        },
        {
          "contestant": 3,
          "problem": 1,
          "time": 11,
          "status": "C"
        },
        {
          "contestant": 1,
          "problem": 2,
          "time": 19,
          "status": "R"
        },
        {
          "contestant": 1,
          "problem": 2,
          "time": 21,
          "status": "C"
        },
        {
          "contestant": 1,
          "problem": 1,
          "time": 25,
          "status": "C"
        }
      ]
    });

    caseObj.generateRanking();

    expect(caseObj.ranking);
    expect(caseObj.ranking[0]).toEqual({
      "contestant": 1,
      "penalty_time": 66,
      "problems_solved": 2
    });
    expect(caseObj.ranking[1]).toMatchObject({
      "contestant": 3,
      "penalty_time": 11,
      "problems_solved": 1
    });

  });

  it("should rank scores, example 2", async () => {
    const caseObj = new Case({
      ranking: [],
      entries: [
        {
          "contestant": 1,
          "problem": 1,
          "time": 10,
          "status": "C"
        },
        {
          "contestant": 2,
          "problem": 2,
          "time": 12,
          "status": "I"
        },
        {
          "contestant": 2,
          "problem": 1,
          "time": 15,
          "status": "C"
        },
        {
          "contestant": 1,
          "problem": 2,
          "time": 20,
          "status": "R"
        },
        {
          "contestant": 3,
          "problem": 2,
          "time": 25,
          "status": "C"
        }
      ]
    });

    caseObj.generateRanking();

    expect(caseObj.ranking[0]).toEqual({
      "contestant": 1,
      "penalty_time": 10,
      "problems_solved": 1
    });
    expect(caseObj.ranking[1]).toMatchObject({
      "contestant": 3,
      "penalty_time": 25,
      "problems_solved": 1
    });
    expect(caseObj.ranking[2]).toMatchObject({
      "contestant": 2,
      "penalty_time": 35,
      "problems_solved": 1
    });
  });
});