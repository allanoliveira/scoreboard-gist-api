# Score board / Gist API

## Requirements
* Docker (https://www.docker.com/get-started)
* Docker Compose (https://docs.docker.com/compose/)

## Folder structure
* `.vscode/` Visual Studio settings
* `bin/` shell scripts for convenience
* `docs/` Documentation as Postman collection
* `src/` Source typescript files to be compiled
    * `entity/` Model definitions
    * `routes/` App routes
* `tests/` Jest tests

## How to use it
* Copy `.env.example` to `.env` and change environment variables accordingly
* Create the database data directory and set the path as `MONGO_DATA_DIR` in the `.env` file.
* Run `docker-compose up --build` for development
    - Changes made to `src/` are live reloaded
* API available at http://localhost:${APP_PORT}/
* Run `docker-compose down` to stop containers and remove containers and networks

### How to test
* For running tests in watch mode, run `./bin/watch-test.sh`
* For running tests in standard mode, run `./bin/test.sh` (useful for CI)
* Make sure you run `docker-compose down` after each usage for cleaning up docker related data

